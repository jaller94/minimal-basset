module.exports = {
  // Root dir for all tests
  roots: ['<rootDir>/..'],

  // Glob paths of all tests, relative to roots
  testMatch: ['**/(*).test.js'],

  // Start services
  globalSetup: '<rootDir>/setup.js',

  // Inject functions for easier testing
  testEnvironment: '<rootDir>/environment.js',

  // Populate and clean data stores like a database
  //setupFilesAfterEnv: ['<rootDir>/setup-tests.js'],

  // Shutdown services and remove temp files
  //globalTeardown: '<rootDir>/teardown.js',
};
