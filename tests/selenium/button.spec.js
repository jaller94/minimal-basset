describe('homepage', async () => {
  test('button test', async () => {
    await driver.get(`http://localhost:${address.port}/`);
    const label = await findByTestId('label-eh');
    const button = await findByTestId('button-eh');
    await snapshot('Home page - No clicks', { widths: '1280' });
    expect(await label.getText()).toBe('0');
    await button.click();
    await button.click();
    expect(await label.getText()).toBe('2');
    await snapshot('Home page - 2 clicks', { widths: '1280' });
  });
});
